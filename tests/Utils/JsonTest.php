<?php declare(strict_types=1);

namespace HttpServiceClient\Test\Utils;

use HttpServiceClient\Exception\InvalidDataException;
use HttpServiceClient\Utils\Json;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase
{
    #[DataProvider('validJsonEncodeDataProvider')]
    public function testValidJsonEncode(array $value, string $expectedResult): void
    {
        $actualResult = Json::encode($value);
        $this->assertEquals($expectedResult, $actualResult);
    }

    #[DataProvider('validJsonDecodeDataProvider')]
    public function testValidJsonDecode(string $json, array $expectedResult): void
    {
        $actualResult = Json::decode($json);
        $this->assertEquals($expectedResult, $actualResult);
    }

    #[DataProvider('invalidJsonEncodeDataProvider')]
    public function testInvalidJsonEncode(array $value): void
    {
        $this->expectException(InvalidDataException::class);
        Json::encode($value);
    }

    #[DataProvider('invalidJsonDecodeDataProvider')]
    public function testInvalidJsonDecode(string $json): void
    {
        $this->expectException(InvalidDataException::class);
        Json::decode($json);
    }

    public static function validJsonEncodeDataProvider(): array
    {
        return [
            [
                [],
                '[]',
            ],
            [
                [''],
                '[""]',
            ],
            [
                ['id' => 1, 'name' => 'Vasia', 'text' => 'Hello World!'],
                '{"id":1,"name":"Vasia","text":"Hello World!"}',
            ],
        ];
    }

    public static function validJsonDecodeDataProvider(): array
    {
        return [
            [
                '{}',
                [],
            ],
            [
                '[""]',
                [''],
            ],
            [
                '{"id":1,"name":"Vasia","text":"Hello World!"}',
                ['id' => 1, 'name' => 'Vasia', 'text' => 'Hello World!'],
            ],
        ];
    }

    public static function invalidJsonEncodeDataProvider(): array
    {
        return [
            [
                ["\xB1\x31"],
            ],
            [
                [NAN],
            ],
            [
                [INF],
            ],
        ];
    }

    public static function invalidJsonDecodeDataProvider(): array
    {
        return [
            [''],
            ['{]'],
            ['{"id":1,"name":\'Vasia\',"text":"Hello World!"}'],
        ];
    }
}
