<?php declare(strict_types=1);

namespace HttpServiceClient\Test\Utils;

use HttpServiceClient\Utils\Uri;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class UriTest extends TestCase
{
    #[DataProvider('joinDataProvider')]
    public function testJoin(array $fragmentsUri, string $expectedResult): void
    {
        $actualResult = Uri::join(...$fragmentsUri);
        $this->assertEquals($expectedResult, $actualResult);
    }

    public static function joinDataProvider(): array
    {
        return [
            [
                ['https://example.com/'],
                'https://example.com'
            ],
            [
                ['https://example.com'],
                'https://example.com'
            ],
            [
                ['https://example.com/', '/comments/'],
                'https://example.com/comments'
            ],
            [
                ['https://example.com', 'comments'],
                'https://example.com/comments'
            ],
            [
                ['https://example.com/', '/comment/', '/7/'],
                'https://example.com/comment/7'
            ],
            [
                ['https://example.com', 'comment', '7'],
                'https://example.com/comment/7'
            ],
            [
                ["https://example.com ", "comment\n", "\t7"],
                'https://example.com/comment/7'
            ],
        ];
    }
}
