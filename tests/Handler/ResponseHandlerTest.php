<?php declare(strict_types=1);

namespace HttpServiceClient\Test\Handler;

use GuzzleHttp\Psr7\Response;
use HttpServiceClient\Exception\BadResponseException;
use HttpServiceClient\Handler\ResponseHandler;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ResponseHandlerTest extends TestCase
{
    public function testResponseHandlerFailed(): void
    {
        $this->expectException(BadResponseException::class);

        new ResponseHandler(
            new Response(300)
        );
    }

    public function testGetStatusCode(): void
    {
        $response = new ResponseHandler(
            new Response(299)
        );

        $this->assertEquals(299, $response->getStatusCode());
    }

    public function testGetBody(): void
    {
        $response = new ResponseHandler(
            new Response(body: 'Subaru')
        );

        $this->assertEquals('Subaru', $response->getBody());
    }

    #[DataProvider('getBodyArrayDataProvider')]
    public function testGetBodyArray(string $body, array $expectedResult): void
    {
        $response = new ResponseHandler(
            new Response(body: $body)
        );

        $this->assertEquals($expectedResult, $response->getBodyArray());
    }

    public static function getBodyArrayDataProvider(): array
    {
        return [
            [
                '',
                []
            ],
            [
                '{}',
                []
            ],
            [
                '{"id":7,"name":"Veronika","text":"Hello World!"}',
                ['id' => 7, 'name' => 'Veronika', 'text' => 'Hello World!']
            ],
        ];
    }
}
