<?php declare(strict_types=1);

namespace HttpServiceClient\Test\Handler;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use HttpServiceClient\Handler\RequestHandler;
use PHPUnit\Framework\TestCase;

class RequestHandlerTest extends TestCase
{
    public function testMethodGet(): void
    {
        $request = new RequestHandler(
            'https://example.com',
            $this->getHttpClient(200, '{"test":"test"}')
        );

        $response = $request->get('/test');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"test":"test"}', $response->getBody());
    }

    public function testMethodPost(): void
    {
        $request = new RequestHandler(
            'https://example.com',
            $this->getHttpClient(201, '{"test":"test"}')
        );

        $response = $request->post('/test', ['test' => 'test']);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('{"test":"test"}', $response->getBody());
    }

    public function testMethodPut(): void
    {
        $request = new RequestHandler(
            'https://example.com',
            $this->getHttpClient(201, '{"test":"test"}')
        );

        $response = $request->put('/test/1', ['test' => 'test']);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('{"test":"test"}', $response->getBody());
    }

    public function testMethodDelete(): void
    {
        $request = new RequestHandler(
            'https://example.com',
            $this->getHttpClient(204, '')
        );

        $response = $request->delete('/test/1');

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getBody());
    }

    private function getHttpClient(int $code, string $body): Client
    {
        $handlerStack = HandlerStack::create(new MockHandler([
            new Response(status: $code, body: $body)
        ]));

        return new Client(['handler' => $handlerStack]);
    }
}
