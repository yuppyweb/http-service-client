<?php declare(strict_types=1);

namespace HttpServiceClient\Test\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Utils;
use HttpServiceClient\Client\Comments;
use HttpServiceClient\Exception\BadResponseException;
use HttpServiceClient\Handler\RequestHandler;
use HttpServiceClient\Model\Comment;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CommentsTest extends TestCase
{
    #[DataProvider('commentDataProvider')]
    public function testGetComment(array $body, Comment $expectedResult): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(200, Utils::jsonEncode($body))
            )
        );

        $actualResult = $comments->get(new Comment($body['id']));

        $this->assertEquals($actualResult, $expectedResult);
    }

    public function testGetCommentFailed(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(404, '')
            )
        );

        $this->expectException(BadResponseException::class);
        $this->expectExceptionCode(404);

        $comments->get(new Comment());
    }

    #[DataProvider('commentsDataProvider')]
    public function testGetComments(array $body, array $expectedResult): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(200, Utils::jsonEncode($body))
            )
        );

        $actualResult = $comments->getAll();

        $this->assertEquals($actualResult, $expectedResult);
    }

    public function testGetCommentsFailed(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(401, '')
            )
        );

        $this->expectException(BadResponseException::class);
        $this->expectExceptionCode(401);

        $comments->getAll();
    }

    #[DataProvider('commentDataProvider')]
    public function testAddComment(array $body, Comment $expectedResult): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(201, Utils::jsonEncode($body))
            )
        );

        $actualResult = $comments->create(
            new Comment(name: $body['name'], text: $body['text'])
        );

        $this->assertEquals($actualResult, $expectedResult);
    }

    public function testAddCommentFailed(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(200, '')
            )
        );

        $this->expectException(BadResponseException::class);
        $this->expectExceptionCode(200);

        $comments->create(new Comment());
    }

    #[DataProvider('commentDataProvider')]
    public function testUpdateComment(array $body, Comment $expectedResult): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(201, Utils::jsonEncode($body))
            )
        );

        $actualResult = $comments->update(
            new Comment($body['id'], $body['name'], $body['text'])
        );

        $this->assertEquals($actualResult, $expectedResult);
    }

    public function testUpdateCommentFailed(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(301, '')
            )
        );

        $this->expectException(BadResponseException::class);
        $this->expectExceptionCode(301);

        $comments->update(new Comment());
    }

    public function testDeleteComment(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(204, '')
            )
        );

        $comments->delete(new Comment(1));

        $this->assertTrue(true);
    }

    public function testDeleteCommentFailed(): void
    {
        $comments = new Comments(
            new RequestHandler(
                'https://example.com',
                $this->getHttpClient(302, '')
            )
        );

        $this->expectException(BadResponseException::class);
        $this->expectExceptionCode(302);

        $comments->delete(new Comment());
    }

    private function getHttpClient(int $code, string $body): Client
    {
        $handlerStack = HandlerStack::create(new MockHandler([
            new Response(status: $code, body: $body)
        ]));

        return new Client(['handler' => $handlerStack]);
    }

    public static function commentDataProvider(): array
    {
        return [
            [
                ['id' => 1, 'name' => 'Colin McRae', 'text' => 'Comment 1'],
                new Comment(1, 'Colin McRae', 'Comment 1')
            ],
            [
                ['id' => 2, 'name' => 'Ken Block', 'text' => 'Comment 2'],
                new Comment(2, 'Ken Block', 'Comment 2')
            ]
        ];
    }

    public static function commentsDataProvider(): array
    {
        return [
            [
                [],
                []
            ],
            [
                [
                    ['id' => 1, 'name' => 'Colin McRae', 'text' => 'Comment 1'],
                    ['id' => 2, 'name' => 'Ken Block', 'text' => 'Comment 2'],
                ],
                [
                    new Comment(1, 'Colin McRae', 'Comment 1'),
                    new Comment(2, 'Ken Block', 'Comment 2')
                ]
            ]
        ];
    }
}
