## Installing
```
composer config minimum-stability dev
```
```
composer require yuppyweb/http-service-client
```

## How to use

```
use GuzzleHttp\Client;
use HttpServiceClient\Client\Comments;
use HttpServiceClient\Handler\RequestHandler;
use HttpServiceClient\Model\Comment;

try {
    $comments = new Comments(
        new RequestHandler(
            'https://example.com',
            new Client()
        )
    );

    $comments->get(new Comment(1));
    $comments->getAll();
    $comments->create(new Comment(name: 'name', text: 'text'));
    $comments->update(new Comment(1, 'name', 'text'));
    $comments->delete(new Comment(1));
} catch (\Throwable $t) {
    
}
```

## Run tests

```
composer test
```
