<?php declare(strict_types=1);

namespace HttpServiceClient\Model;

readonly class Comment
{
    public function __construct(
        public ?int $id = null,
        public ?string $name = null,
        public ?string $text = null
    ) {}
}
