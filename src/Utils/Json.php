<?php declare(strict_types=1);

namespace HttpServiceClient\Utils;

use HttpServiceClient\Exception\InvalidDataException;
use JsonException;

class Json
{
    /**
     * @throws InvalidDataException
     */
    public static function encode(array $value): string
    {
        try {
            return json_encode($value, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new InvalidDataException(
                "Invalid data for encoding: {$exception->getMessage()}",
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * @throws InvalidDataException
     */
    public static function decode(string $json): array
    {
        try {
            return json_decode(json: $json, flags: JSON_OBJECT_AS_ARRAY|JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new InvalidDataException(
                "Invalid data for decoding: {$exception->getMessage()}",
                $exception->getCode(),
                $exception
            );
        }
    }
}
