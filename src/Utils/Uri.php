<?php declare(strict_types=1);

namespace HttpServiceClient\Utils;

class Uri
{
    public static function join(string ...$fragments): string
    {
        return implode('/', array_map(
            fn(string $fragment) => trim($fragment, " \t\n\r\0\x0B/"),
            $fragments
        ));
    }
}
