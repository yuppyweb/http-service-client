<?php declare(strict_types=1);

namespace HttpServiceClient\Client;

use HttpServiceClient\Exception\BadResponseException;
use HttpServiceClient\Exception\InvalidDataException;
use HttpServiceClient\Handler\RequestHandler;
use HttpServiceClient\Model\Comment;
use Psr\Http\Client\ClientExceptionInterface;

readonly class Comments
{
    public function __construct(
        private RequestHandler $request
    ) {}

    /**
     * @throws BadResponseException
     * @throws ClientExceptionInterface
     * @throws InvalidDataException
     */
    public function get(Comment $comment): Comment
    {
        $response = $this->request->get("/comment/{$comment->id}");

        if ($response->getStatusCode() !== 200) {
            throw new BadResponseException(
                $response->getBody(),
                $response->getStatusCode()
            );
        }

        return $this->createCommentFromArray(
            $response->getBodyArray()
        );
    }

    /**
     * @return Comment[]
     * @throws BadResponseException
     * @throws InvalidDataException
     * @throws ClientExceptionInterface
     */
    public function getAll(): array
    {
        $response = $this->request->get("/comments");

        if ($response->getStatusCode() !== 200) {
            throw new BadResponseException(
                $response->getBody(),
                $response->getStatusCode()
            );
        }

        return array_map(
            fn(array $comment): Comment => $this->createCommentFromArray($comment),
            $response->getBodyArray()
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws BadResponseException
     * @throws InvalidDataException
     */
    public function create(Comment $comment): Comment
    {
        $response = $this->request->post('/comment', [
            'name' => $comment->name,
            'text' => $comment->text
        ]);

        if ($response->getStatusCode() !== 201) {
            throw new BadResponseException(
                $response->getBody(),
                $response->getStatusCode()
            );
        }

        return $this->createCommentFromArray(
            $response->getBodyArray()
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws BadResponseException
     * @throws InvalidDataException
     */
    public function update(Comment $comment): Comment
    {
        $response = $this->request->put("/comment/{$comment->id}", [
            'name' => $comment->name,
            'text' => $comment->text
        ]);

        if ($response->getStatusCode() !== 201) {
            throw new BadResponseException(
                $response->getBody(),
                $response->getStatusCode()
            );
        }

        return $this->createCommentFromArray(
            $response->getBodyArray()
        );
    }

    /**
     * @throws BadResponseException
     * @throws ClientExceptionInterface
     * @throws InvalidDataException
     */
    public function delete(Comment $comment): void
    {
        $response = $this->request->delete("/comment/{$comment->id}");

        if ($response->getStatusCode() !== 204) {
            throw new BadResponseException(
                $response->getBody(),
                $response->getStatusCode()
            );
        }
    }

    private function createCommentFromArray(array $comment): Comment
    {
        return new Comment(
            $comment['id'] ?? null,
            $comment['name'] ?? null,
            $comment['text'] ?? null
        );
    }
}
