<?php declare(strict_types=1);

namespace HttpServiceClient\Handler;

use HttpServiceClient\Exception\BadResponseException;
use HttpServiceClient\Exception\InvalidDataException;
use HttpServiceClient\Utils\Json;
use Psr\Http\Message\ResponseInterface;

readonly class ResponseHandler
{
    /**
     * @throws BadResponseException
     */
    public function __construct(
        private ResponseInterface $response
    ) {
        if ($this->response->getStatusCode() > 299) {
            throw new BadResponseException(
                $this->response->getBody()->getContents(),
                $this->response->getStatusCode()
            );
        }
    }

    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    public function getBody(): string
    {
        return $this->response->getBody()->getContents();
    }

    /**
     * @throws InvalidDataException
     */
    public function getBodyArray(): array
    {
        $body = $this->getBody();

        if (empty($body)) {
            return [];
        }

        return Json::decode($body);
    }
}
