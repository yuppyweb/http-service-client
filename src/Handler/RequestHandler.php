<?php declare(strict_types=1);

namespace HttpServiceClient\Handler;

use GuzzleHttp\Psr7\Request;
use HttpServiceClient\Exception\BadResponseException;
use HttpServiceClient\Exception\InvalidDataException;
use HttpServiceClient\Utils\Json;
use HttpServiceClient\Utils\Uri;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;

readonly class RequestHandler
{
    public function __construct(
        private string $baseUri,
        private ClientInterface $httpClient
    ) {}

    /**
     * @throws ClientExceptionInterface
     * @throws BadResponseException
     * @throws InvalidDataException
     */
    public function get(string $uri): ResponseHandler
    {
        return $this->send('GET', $uri);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws BadResponseException
     * @throws InvalidDataException
     */
    public function post(string $uri, array $body = null): ResponseHandler
    {
        return $this->send('POST', $uri, $body);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws BadResponseException
     * @throws InvalidDataException
     */
    public function put(string $uri, array $body = null): ResponseHandler
    {
        return $this->send('PUT', $uri, $body);
    }

    /**
     * @throws BadResponseException
     * @throws InvalidDataException
     * @throws ClientExceptionInterface
     */
    public function delete(string $uri): ResponseHandler
    {
        return $this->send('DELETE', $uri);
    }

    /**
     * @throws BadResponseException
     * @throws ClientExceptionInterface
     * @throws InvalidDataException
     */
    private function send(string $method, string $uri, ?array $body = null): ResponseHandler
    {
        $request = new Request(
            method: $method,
            uri: Uri::join($this->baseUri, $uri),
            body: !empty($body) ? Json::encode($body) : null
        );

        return new ResponseHandler(
            $this->httpClient->sendRequest($request)
        );
    }
}
